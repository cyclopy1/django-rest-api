from rest_framework.pagination import PageNumberPagination


class TinyResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'psize'
    max_page_size = 10
