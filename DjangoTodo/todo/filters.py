import django_filters

from .models import Todo


class TodoFilter(django_filters.FilterSet):
    ids = django_filters.BaseInFilter(field_name='id')

    class Meta():
        model = Todo
        fields = {
            'title': ('icontains',),
            'description': ('icontains',),
            'finished': ('exact',)
        }
