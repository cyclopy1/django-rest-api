from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Todo(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=False)

    title = models.CharField(max_length=150, null=False, blank=False)
    description = models.TextField(null=True)
    finished = models.BooleanField(default=False)
