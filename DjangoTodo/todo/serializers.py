from typing import Any, Dict

from rest_framework.serializers import ModelSerializer

from .models import Todo


class TodoSerializer(ModelSerializer):
    class Meta:
        model = Todo
        fields = ('id', 'title', 'description', 'finished')

    def create(self, validated_data: Dict[str, Any]):
        """Update the validated data and call the super.create function.
        
        Update the validated data with the owner that is the user that made the
        request.
        """
        owner = self.context['request'].user
        validated_data['owner'] = owner
        return super().create(validated_data)
