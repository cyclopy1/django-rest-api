from rest_framework import viewsets

from .filters import TodoFilter
from .models import Todo
from .pagination import TinyResultsSetPagination
from .serializers import TodoSerializer


# Create your views here.
class TodoViewset(viewsets.ModelViewSet):

    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    filter_class = TodoFilter
    pagination_class = TinyResultsSetPagination

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(owner=self.request.user)
